#! /bin/bash

echo '$# gives $ of arguments     :' $#
echo '$@ gives arguments as array :' $@
echo '$* gives arguments as string:' $*

echo '$1, $2, $3 give first, second, third arguments etc'
echo '$1:' $1
echo '$2:' $2
echo '$2:' $3

echo 'Evaluating "$@"'
for ARG in "$@"
do
	    echo ${ARG}
    done
    echo 'Evaluating "$*"'
    for ARG in $*
    do
	        echo ${ARG}
	done

