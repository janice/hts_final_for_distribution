# Copyright (c) Jupyter Development Team.
# Distributed under the terms of the Modified BSD License.

FROM dukehtscourse/jupyter-hts-2019

# Notebooks were not being copied into container by Binder

USER jovyan
RUN git clone https://gitlab.oit.duke.edu/janice/hts_final_for_distribution
